``#!/usr/bin/env node``

fs = require 'fs'
path = require 'path'

{ lines, filter, words, keys, map, each, join, compact, last, values, } = prelude-ls = require 'prelude-ls'

fish-lib = require 'fish-lib'
    ..import-kind global, 'all'

sys-set { +die, -verbose, +sync, }

{ argv, exit, } = process

slurp = (f) -->
    fs.read-file-sync f .to-string()

e = (f) ->
    fs.exists-sync f

dir = (l, file) -->
    m = if l then
        '../' * that
    else void
    join '' compact [m, file]

prepend = (xs, ys) -->
    xs = [xs] unless is-array xs
    xs ++ ys

config =
    usage: 'pattern [-d] [-i] [-c]'
    file: '.fish-tree-files'

opt = getopt do
    h:  'b'
    d:  'b'
    i:  'b'
    c:  'b'

our =
    debug: opt.d
    ignore-case: opt.i or false
    color: opt.c or false

sys-set verbose: true if our.debug

[pattern, ...xxx] = opt.argv.remain
error usage() if not pattern or xxx.length

if opt.h
    info usage()
    exit 0

function usage
    [_, script-name, ...args] = argv
    str = join ' ' compact array script-name, config.usage
    sprintf "Usage: %s" str

go pattern

function go pattern
    { file: cfile, } = config
    f = find-file cfile
    error "Can't find" bright-red cfile unless f
    { levels, file, } = f
    info 'got' (green file), yellow levels if our.debug
    sys-spawn do
        'grep'
        args file, levels, pattern
        out-print: true

function args file, levels, pattern
    slurp file
    |> lines
    |> (x) ->
        error "empty" unless x.length
        x
    |> filter (!= '')
    |> map dir levels
    |> (x) ->
        flag = if our.ignore-case then ['-i'] else []
        prepend flag, x
    |> prepend ['-Pn' pattern]
    |> (x) ->
        flag = if our.color then ['--color=always'] else []
        prepend flag, x

function find-file file
    l = 0
    while not e (f = dir l, file)
        info 'tried ' f if our.debug
        l = l + 1
        return if (path.resolve '.' path.dirname f) == '/'
    levels: l
    file: f

